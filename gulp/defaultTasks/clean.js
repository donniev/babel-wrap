'use strict';
var pth = require("path");
var del = require('del');
module.exports = (gulp, plugins, paths)=>  ()=>del([
	pth.resolve(paths.dest) + "/**/*"
], {force: true});

