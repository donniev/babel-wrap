'use strict';
module.exports = (gulp, plugins, paths)=>  ()=> {
	if (paths["debug"]) {
		return gulp.src(paths.src.copyIncludes, {base: paths.base}).pipe(plugins.debug()).pipe(gulp.dest(paths.dest));
	} else {
		return gulp.src(paths.src.copyIncludes, {base: paths.base}).pipe(gulp.dest(paths.dest));
	}
};