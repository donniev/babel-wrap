'use strict';
module.exports = function (gulp, plugins, paths) {
	return function () {
		if (paths["debug"]) {
			return gulp.src(paths.src.copy, {base: paths.base}).pipe(plugins.debug()).pipe(gulp.dest(paths.dest));
		} else {
			return gulp.src(paths.src.copy, {base: paths.base}).pipe(gulp.dest(paths.dest));
		}
	};
};