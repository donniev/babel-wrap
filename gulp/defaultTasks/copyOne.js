'use strict';
module.exports = function (gulp, plugins, paths, source) {
	return function () {
		var sourceFile = void 0;
		switch (source) {
			case "server":
			{
				sourceFile =__dirname+ "/../serverConfig.json";
				//console.log(sourceFile);
				break;
			}
			case "server4":
			{
				sourceFile =__dirname+ "/../serverConfigv4.json";
				break;
			}
			case "server5":
			{
				sourceFile =__dirname+ "/../serverConfigv5.json";
				break;
			}
			case "client":
			{
				sourceFile =__dirname+ "/../clientConfig.json";
				break;
			}
			default:
			{
				sourceFile = "./gulp/paths.json";
			}
		}
		if (paths["debug"]) {
			return gulp.src(sourceFile).pipe(plugins.debug()).pipe(gulp.dest(".")).pipe(plugins.debug());
		} else {
			return gulp.src(sourceFile).pipe(gulp.dest("."));
		}
	};
};
//# sourceMappingURL=copyOne.js.map
