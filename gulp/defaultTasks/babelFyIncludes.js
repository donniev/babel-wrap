/**
 * module babelFyIncludesOverRide
 *
 */
'use strict';
var minimist = require('minimist')(process.argv.slice(2));
module.exports = (gulp, plugins, paths)=>   () => {
	if (paths.babeloptions && paths.babeloptions.sourceMaps) {
		if (paths["debug"]) {
			return gulp.src(paths.src.babelFyIncludes, {base: paths.base})
				.pipe(plugins.sourcemaps.init())
				.pipe(plugins.babel(paths.babeloptions))
				.pipe(plugins.sourcemaps.write('.'))
				.pipe(plugins.debug())
				.pipe(gulp.dest(paths.dest));
		} else {
			return gulp.src(paths.src.babelFyIncludes, {base: paths.base})
				.pipe(plugins.sourcemaps.init())
				.pipe(plugins.babel(paths.babeloptions))
				.pipe(plugins.sourcemaps.write('.'))
				.pipe(gulp.dest(paths.dest));
		}

	} else {
		if (paths["debug"]) {
			return gulp.src(paths.src.babelFyIncludes, {base: paths.base})
				.pipe(plugins.babel(paths.babeloptions))
				.pipe(plugins.debug())
				.pipe(gulp.dest(paths.dest));
		} else {
			return gulp.src(paths.src.babelFyIncludes, {base: paths.base})
				.pipe(plugins.babel(paths.babeloptions))
				.pipe(gulp.dest(paths.dest));
		}
	}
};
