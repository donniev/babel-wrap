/**
 * module postinstall
 *
 */
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	path = require("path");
let targetDirectory = path.resolve(__dirname, "../..");
let args = require("minimist")(process.argv);
let sourceFile = void(0);
let source = args.target;
let overwrite=args.overwrite==="true";
switch (source) {
	case "server":
	{
		sourceFile = "./gulp/serverConfig.json";
		break;
	}
	case "server4":
	{
		sourceFile = "./gulp/serverConfigv4.json";
		break;
	}
	case "client":
	{
		sourceFile = "./gulp/clientConfig.json";
		break;
	}
	default:
	{
		sourceFile = "./gulp/paths.json";
	}
}
let copyfile = ()=>fs.createReadStream(sourceFile).pipe(fs.createWriteStream(targetDirectory + "/babelwrapConfig.json"));
fs.readFileAsync(targetDirectory + "/babelwrapConfig.json", "utf8")
	.then((results)=>{
		if(overwrite){
			copyfile()
		}
	})
	.catch((err)=> copyfile()); //error is good cannot find config file


