'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var paths = require('./setPaths')();
gulp.on("error", (err)=> {
	console.log("Oops: %s", err);
	throw err;
});
gulp.task('clean', require('./gulp/defaultTasks/clean')(gulp, plugins, paths));
gulp.task('copy', require("./gulp/defaultTasks/copy")(gulp, plugins, paths));
gulp.task('babelFy', require('./gulp/defaultTasks/babelFy')(gulp, plugins, paths));
gulp.task('babelFyIncludes', require('./gulp/defaultTasks/babelFyIncludes')(gulp, plugins, paths));
gulp.task('copyIncludes', require('./gulp/defaultTasks/copyIncludes')(gulp, plugins, paths));
gulp.task('watch', (cb)=> {
	runSequence('clean', 'copy', 'copyIncludes', 'babelFy', 'babelFyIncludes', ['watchAll', 'watchBabelFy', 'watchBabelFyIncludes', 'watchCopyIncludes'], cb);
});
gulp.task('watchBabelFy', ()=> {
	var watcher = gulp.watch(paths.src.babelFy, {interval: 500}, (event)=> {
		if (paths.babeloptions.sourceMaps) {
			return gulp.src(event.path, {base: paths.base})
				.pipe(plugins.sourcemaps.init())
				.pipe(plugins.babel(paths.babeloptions))
				.pipe(plugins.sourcemaps.write('.'))
				.pipe(plugins.debug())
				.pipe(gulp.dest(paths.dest));
		} else {
			return gulp.src(event.path, {base: paths.base})
				.pipe(plugins.babel(paths.babeloptions))
				.pipe(plugins.debug())
				.pipe(gulp.dest(paths.dest));
		}
	});
});
gulp.task('watchBabelFyIncludes', ()=> {
	var watcher = gulp.watch(paths.src.babelFyIncludes, {interval: 500}, function (event) {
		if (paths.babeloptions.sourceMaps) {
			if (paths.debug) {
				return gulp.src(event.path, {base: paths.base})
					.pipe(plugins.sourcemaps.init())
					.pipe(plugins.babel(paths.babeloptions))
					.pipe(plugins.sourcemaps.write('.'))
					.pipe(plugins.debug())
					.pipe(gulp.dest(paths.dest));
			} else {
				return gulp.src(event.path, {base: paths.base})
					.pipe(plugins.sourcemaps.init())
					.pipe(plugins.babel(paths.babeloptions))
					.pipe(plugins.sourcemaps.write('.'))
					.pipe(gulp.dest(paths.dest));
			}
		} else {
			if (paths.debug) {
				return gulp.src(event.path, {base: paths.base})
					.pipe(plugins.babel(paths.babeloptions))
					.pipe(plugins.debug())
					.pipe(gulp.dest(paths.dest));
			} else {
				return gulp.src(event.path, {base: paths.base})
					.pipe(plugins.babel(paths.babeloptions))
					.pipe(gulp.dest(paths.dest));
			}
		}
	});
});
gulp.task('watchAll', ()=> {
	gulp.watch(paths.src.nowatch, {interval: 500}, function (event) {
		if (paths.debug) {
			gulp.src(event.path, {base: paths.base}).pipe(plugins.debug()).pipe(gulp.dest(paths.dest));
		} else {
			gulp.src(event.path, {base: paths.base}).pipe(gulp.dest(paths.dest));
		}
	});
});
gulp.task('watchCopyIncludes', ()=> {
	gulp.watch(paths.src.copyIncludes, {interval: 500}, function (event) {
		if (paths.debug) {
			gulp.src(event.path, {base: paths.base}).pipe(plugins.debug()).pipe(gulp.dest(paths.dest));
		} else {
			gulp.src(event.path, {base: paths.base}).pipe(gulp.dest(paths.dest));
		}
	});
});
gulp.task('default', ['watch']);
