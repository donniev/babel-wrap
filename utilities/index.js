/**
 * module index
 *
 */
module.exports = {
	endsWith: (st, phrase)=> {
		"use strict";
		if (Object.prototype.toString.call(String.prototype.endsWith) === '[object Function]') {
			return st.endsWith(phrase);
		} else {
			return st.lastIndexOf(phrase) === (st.length - phrase.length)
		}
	},
	startsWith: (st, phrase)=> {
		"use strict";
		if (Object.prototype.toString.call(String.prototype.startsWith) === '[object Function]') {
			return st.startsWith(phrase);
		} else {
			return st.indexOf(phrase) === 0
		}
	}
}
