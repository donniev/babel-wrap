## 2015/10/1 DLV 0.0.1 
1. initital commit
## 2015/10/11 DLV 0.0.1-1 
1. clean up
## 2015/10/11 DLV 0.0.1-2
1. made base configurable
1. replaced all "../../.." with paths.base
## 2015/10/11 DLV 0.0.1-3
1. can now pass in babeloptions
1. default it to not babelfy generators
## 2015/10/12 DLV 0.0.1-4
1. oopsie in del
## 2015/10/12 DLV 0.0.1-5
1. added babelFyIncludes to override default excludes
## 2015/10/14 DLV 0.0.1-6
1. debug optional
## 2015/10/15 DLV 0.0.1-7
1. updated README
## 2015/10/16 DLV 0.0.2
1. simplified and added debug flag
## 2015/10/16 DLV 0.0.2
1. added copyIncludes
## 2015/10/16 DLV 0.0.3
1. You can now use within another gulp file
## 2015/10/17 DLV 0.0.3
1. Better namespacing
## 2015/10/19 DLV 0.0.3
1. run in sequence issue
## 2015/10/21 KSC 0.0.2 (renamed to gulp-babel-wrap)
1. No not overwrite babelwrapConfig if the file exists.
## 2015/10/21 DLV 0.0.2 
1. Clean up of postinstall. No need to make backup since we are not overwriting
## 2015/10/21 DLV 0.2.0 
1. Added multiple instances
1. Added config file options
1. Added default configurations for server, serverv4 and client
1. added options --target and --overwrite to npm install
## 2015/10/23 DLV 0.2.1
1. Changed read me for correct install instructions
## 2015/10/23 DLV 0.2.2
1. Added copy tasks
## 2015/10/24 DLV 0.2.3
1. added jasmine unit tests
## 2015/11/01 DLV 0.3.0
1. Added copy server for node5. Does not transpile spread operator

