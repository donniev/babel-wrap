#Babel Wrap


Babel wrap enables you to automate running the babel transpiler on you code
without have to create all the gulp tasks to handle it. You can run the tasks
directly from the module but it is easier to just make the tasks available
in your projects gulpfile.js, You can have multiple instances in your gulp file
so you can transpile client code differently than server code.

##Features:
1. Automatically copies compiled code to a destination directory.
1. Automatically excludes node_modules.
1. Can override default files to process via configuration.
1. Can overide default babel options via configuration.
1. Can transpile client code differently than server code.

##Installation

```
npm install gulp-babel-wrap  --save-dev  && cd node_modules/gulp-babel-wrap  && npm run postinstall -- --target server4 --overwrite true && cd ../..

```
If you know you are going to use a particular configuration the above works fine.
If you are going to use multiple configurations it is easier to just do:

```
npm install gulp-babel-wrap --save-dev
```
which loads the default configuration. You can then copy the needed config files by running the gulp tasks (copyServer, copyServerv4 or copyClient)

Target:

1. server4 - do not transpile features that node version 4+ handles natively
1. server - do not transpile features that node v0.12+ handles natively (with harmony flag)
1. client - transpile everything

Overwrite:

Installation will copy the appropriate configuration file to babelwrapConfig.json in the root
of your project. It will not overwrite an existing file unless you set overwrite to true



##Usage
1. Available tasks:
	1. clean - wipes the destination directory (default 'dist'
	1. copy - copies files to destination directory (CAUTION: used alone this will not transpile the files first; by default node_modules is not copied)
	1. babelFy - transpiles the code any copies to the destination
	1. babelFyIncludes - transpiles the code that would other be exluded by a wildcard exclusion
	1. copyIncludes - copies the code that would other be exluded by a wildcard exclusion
	1. watchBabelFy - executes babelFy when a file changes
	1. watchBabelFyIncludes - executes babelFyIncludes when a file changes
	1. watchCopyIncludes - copies file when a file changes
	1. watchAll - copies non transpiled code when changed (Does not watch node_modules directory)
	1. watch - (default) runs all the above in the correct sequence.
    1. copyServer - copy configServer.json from node_modules to root of your project
    1. copyServerv4 - copy serverConfig.json from node_modules to root of your project
    1. copyClient - copy clientConfig.json from node_modules to root of your project

1. From command line (you must, of course, have installed gulp globally)
	
	```
	gulp --gulpfile node_modules/gulp-babel-wrap/gulpfile.js  <task>
	```
	
1. From WebStorm
	1. Select gulpfile.js in node_modules/gulp-babel-wrap or in your project's root
	1. Right-click choose Show gulp tasks
	1. In the gulp pane double click the task you want to run

1. Debugging
	1. set debug to true in config file
	1. or run gulp adding --debug after the task
	
1. Using within your project's gulpfile.js

```
var gulp=require("gulp");
var ns="bw";
require("gulp-babel-wrap")(gulp,ns,configFile);

```

1. The tasks above will be available to be incorporated in your flow. They will be prefixed by whatever you have provided as a namespace(2nd argument) to avoid conflicts
1. If configFile is not provided or is null babelwrapConfig.json will be used.
1. If configFile is a string that string should point to the config file (json) you wish to use.
1. If configFile is a json formatted object it will use that.
1. You can use different config files for different parts of your project by repeating the require(...)(...) statement. Just make sure you give each one a different namespace. This is helpful 
if you want to have different babel options for server code than for client code.

```
var gulp=require("gulp");
var ns = "client"
require("gulp-babel-wrap(gulp,ns,require("./clientConfig.json"));
ns=server;
require("gulp-babel-wrap(gulp,ns,require("./serverConfigv4.json"));
```
You then will have two sets of tasks client.xxx and server.xxx.
You can, of course edit the config files you copied and rename them if you like.
BEWARE that running the copyServer,copyServerv4 and copyClient AGAIN will overwrite the existing files.

1. You can also use the command line if you have it in your project's gulpfile

```
gulp <task>  //where task if for example bw.babelFy
```

##Default Configuration

```
{
	"src"         : {
		"copy"           : [
			"**/*",
			".ebextensions/*.config",
			"!dist/**/*",
			"!dist",
			"!node_modules/**/*",
			"!node_modules",
			"!babelwrapConfig.json",
			"!babelwrapConfig.json.bak",
			"!clientConfig.json",
			"!serverConfig.json",
			"!serverConfigv4.json"
			
		],
		"nowatch"        : [
			"!node_modules/**/*.*"
		],
		"babelFy"        : [
			"**/*.js",
			"!**/*.min.js",
			"!dist/**/*.js",
			"!node_modules/**/*.js"
		],
		"babelFyIncludes": [],
		"copyIncludes"   : []
	},
	"dest"        : "dist",
	"base"        : "",
	"babeloptions": {
		"blacklist" : [
			"bluebirdCoroutines",
			"asyncToGenerator",
			"regenerator",
			"runtime"
		],
		"sourceMaps": true

	},
	"debug"       : false
}
```

## Client default configuration

```
{
	"src"         : {
		"copy"           : [
			"**/*",
			".ebextensions/*.config",
			"!dist/**/*",
			"!dist",
			"!node_modules/**/*",
			"!node_modules",
			"!babelwrapConfig.json",
			"!babelwrapConfig.json.bak",
           	"!clientConfig.json",
            "!serverConfig.json",
            "!serverConfigv4.json"
		],
		"nowatch"        : [
			"!node_modules/**/*.*"
		],
		"babelFy"        : [
			"**/*.js",
			"!**/*.min.js",
			"!dist/**/*.js",
			"!node_modules/**/*.js"
		],
		"babelFyIncludes": [],
		"copyIncludes"   : []
	},
	"dest"        : "dist",
	"base"        : "",
	"babeloptions": {
		"blacklist" : [
		],
		"sourceMaps": true

	},
	"debug"       : false
}
```

## Server default configuration (v0.12)

```
{
	"src"         : {
		"copy"           : [
			"**/*",
			".ebextensions/*.config",
			"!dist/**/*",
			"!dist",
			"!node_modules/**/*",
			"!node_modules",
			"!babelwrapConfig.json",
			"!babelwrapConfig.json.bak",
            "!clientConfig.json",
            "!serverConfig.json",
            "!serverConfigv4.json"
		],
		"nowatch"        : [
			"!node_modules/**/*.*"
		],
		"babelFy"        : [
			"**/*.js",
			"!**/*.min.js",
			"!dist/**/*.js",
			"!node_modules/**/*.js"
		],
		"babelFyIncludes": [],
		"copyIncludes"   : []
	},
	"dest"        : "dist",
	"base"        : "",
	"babeloptions": {
		"blacklist" : [
			"bluebirdCoroutines",
			"asyncToGenerator",
			"regenerator",
			"runtime"
		],
		"sourceMaps": true

	},
	"debug"       : false
}
```

## Server default configuration (v4+)

```
{
	"src"         : {
		"copy"           : [
			"**/*",
			".ebextensions/*.config",
			"!dist/**/*",
			"!dist",
			"!node_modules/**/*",
			"!node_modules",
			"!babelwrapConfig.json",
			"!babelwrapConfig.json.bak",
            "!clientConfig.json",
            "!serverConfig.json",
            "!serverConfigv4.json"
		],
		"nowatch"        : [
			"!node_modules/**/*.*"
		],
		"babelFy"        : [
			"**/*.js",
			"!**/*.min.js",
			"!dist/**/*.js",
			"!node_modules/**/*.js"
		],
		"babelFyIncludes": [],
		"copyIncludes"   : []
	},
	"dest"        : "dist",
	"base"        : "",
	"babeloptions": {
		"blacklist" : [
			"bluebirdCoroutines",
			"asyncToGenerator",
			"regenerator",
			"runtime",
			"es6.classes",
			"es6.arrowFunctions",
			"es6.constants",
			"es6.forOf",
			"es6.literals",
			"es6.blockScoping",
			"es6.templateLiterals"
		],
		"sourceMaps": true

	},
	"debug"       : false
}
```
There is now a configuration for v5. The only difference is that it does not transpile the spread operator

##Custom Configuration
Edit babelwrapConfig.json
When you install this file is added to your root directory.
By default thes files are not copied to the distribution folder


## Babeloptions considerations
The default configuration disables the transformers which
require the runtime module (which is not recommended for production by Babel).
The consequence of this is that any code which requires the babel polyfill will
not be "babelfied". If you use this for server side code in node then that works
fine as long as your target version of node provides those features natively. If, however, you are using features
that are implemented in v4.x of node (like Array.from) but which are not implemented in earlier
versions of node then the code will not run in the earlier versions. If you really need the code to
run in those environments you can enable the runtime transformer but, as noted by Babel, this is not
recommended for production. Best practices is to avoid features that require the polyfill unless your target platform implements them natively.
Fortunately node 4.x implements almost everything natively except the React api.
Node with the --harmony flag implements many (let, Generators, iterators, Sets and Maps).
You should, of course, choose the appropriate config file(s) when you install the plugin
based on your use case. You can always modify that as you like. If you are using multiple
config files (say server4 and client) you can copy the defaults which live in the gulp directory to
the root of your project so they can live in source control.


##Change log

0.2.3 Initial release

0.3.0 2015/11/01
1. Added node5 configuration file which does not transpile spread operator. Otherwise identical to 4.






