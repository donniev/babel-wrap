/**
 * module testBW.js
 *
 */
'use strict';
let Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	path = require("path")
let gulp = require("gulp");
let plugins = require('gulp-load-plugins')();
let sourceFile=require(__dirname+"/../gulp/serverConfigv4.json");
let paths = require("../index")(gulp, "bw", sourceFile);
let clean = (destination)=>new Promise((resolve, reject)=> {
	let del = require('del');
	if (typeof destination === 'string') {
	del([
		destination
	], {force: true})
		.then(resolve)
		.catch(reject)
}else{
		//an array
		del(
			destination
		, {force: true})
			.then(resolve)
			.catch(reject)
	}
})
describe("testBabelFyServer", ()=> {
	beforeEach((done)=> {
		clean(path.resolve(paths.dest) + "/**/*")
			.then((results)=> {
				done();
			})
			.catch((err)=> {
				expect(err).toEqual(null);
				done();
			});
	});
	it("babelfyhasfatarrows", (done)=> {
		let foo = require("../gulp/defaultTasks/babelFy");
		let bar = foo(gulp, plugins, paths)();
		bar.on("end", ()=> {
			let dir = path.resolve(paths.dest);
			fs.readdirAsync(dir )
				.then((results)=> {
					expect(results.indexOf("index.js")).toBeGreaterThan(0);
					expect(results.indexOf("index.js.map")).toBeGreaterThan(0);
					fs.readFileAsync(dir+"/index.js","utf8")
					.then((content)=>{
							expect(content.indexOf("=>")).toBeGreaterThan(0);
							done();
						})
						.catch((err)=> {
							console.log(err,err.stack);
							expect(err).toEqual(null);
							done();
						})

				})
				.catch((err)=> {
					expect(err).toEqual(null);
					done();
				});
		});
	});

})