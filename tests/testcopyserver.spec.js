/**
 * module testBW.js
 *
 */
'use strict';
let Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	path = require("path")
let gulp = require("gulp");
let plugins = require('gulp-load-plugins')();
let paths = require("../index")(gulp, "bw", __dirname + "/babelwrapConfig.json");
let clean = (destination)=>new Promise((resolve, reject)=> {
	let del = require('del');
	if (typeof destination === 'string') {
	del([
		destination
	], {force: true})
		.then(resolve)
		.catch(reject)
}else{
		//an array
		del(
			destination
		, {force: true})
			.then(resolve)
			.catch(reject)
	}
})
describe("copyServer", ()=> {
	beforeEach((done)=> {
		clean(path.resolve(paths.dest) + "/**/*")
			.then((results)=> {
				let d = [path.resolve(__dirname, "..", "serverConfigv4.json"),
					path.resolve(__dirname, "..", "serverConfig.json"),
					path.resolve(__dirname, "..", "clientConfigv4.json")]
				clean(d)
					.then((results)=> {
						done();
					})
					.catch((err)=> {
						expect(err).toEqual(null);
						done();
					});
			})
			.catch((err)=> {
				expect(err).toEqual(null);
				done();
			});
	});
	it("copyserver", (done)=> {
		let foo = require("../gulp/defaultTasks/copyOne");
		let bar = foo(gulp, plugins, paths, "server")();
		bar.on("end", ()=> {
			let dir = path.resolve(__dirname, "..");
			fs.readFileAsync(dir + "/serverConfig.json", "utf8")
				.then((results)=> {
					let obj = JSON.parse(results);
					expect(obj.babeloptions.blacklist[0]).toEqual("bluebirdCoroutines");
					done()
				})
				.catch((err)=> {
					expect(err).toEqual(null);
					done();
				})
		});
	});
	it("copyserverv4", (done)=> {
		let foo = require("../gulp/defaultTasks/copyOne");
		let bar = foo(gulp, plugins, paths, "server4")();
		bar.on("end", ()=> {
			let dir = path.resolve(__dirname, "..");
			fs.readFileAsync(dir + "/serverConfigv4.json", "utf8")
				.then((results)=> {
					let obj = JSON.parse(results);
					expect(obj.babeloptions.blacklist[4]).toEqual("es6.classes");
					done()
				})
				.catch((err)=> {
					expect(err).toEqual(null);
					done();
				})
		});
	});
	it("copyserver", (done)=> {
		let foo = require("../gulp/defaultTasks/copyOne");
		let bar = foo(gulp, plugins, paths, "client")();
		bar.on("end", ()=> {
			let dir = path.resolve(__dirname, "..");
			fs.readFileAsync(dir + "/clientConfig.json", "utf8")
				.then((results)=> {
					let obj = JSON.parse(results);
					expect(obj.babeloptions.blacklist.length).toEqual(0);
					done()
				})
				.catch((err)=> {
					expect(err).toEqual(null);
					done();
				})
		});
	});
})