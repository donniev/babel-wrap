/**
 * module setPaths
 *
 */
'use strict';
var pth = require("path");
var u = require("./utilities/index");
/**
 * If configFile is undefined or null use babelwrapConfig.json
 * If a string it is name of the configFile
 * Otherwise it is an anctual jason object
 *
 *
 * @param configFile
 * @returns {*}
 */
module.exports = (configFile)=> {
	let target = void(0);
	let paths = require("./gulp/modulepathsGulp.json");
	let _debug = require('minimist')(process.argv.slice(2)).debug;
	try {
		if ((configFile === void(0)) || (configFile === null)) {
		} else if (typeof(configFile) === 'string') {
			if (!u.endsWith(configFile, ".json")) {
				configFile = configFile + ".json";
			}
			target = require(pth.resolve(__dirname, "../../", configFile))
		} else {
			target = configFile;
		}
		let special = target;
		special = target;
		let regions = ["copy", "nowatch", "babelFy", "babelFyIncludes", "copyIncludes"];
		special["src"]["nowatch"] = special["src"]["copy"].concat(special["src"]["nowatch"]);
		special["debug"] = special["debug"] || _debug;
		return special;
	} catch (ex) {
		if (_debug) {
			console.log(ex);
		}
		paths["src"]["nowatch"] = paths["src"]["copy"].concat(paths["src"]["nowatch"]);
		paths["debug"] = paths["debug"] || _debug;
		return paths
	}
};
//# sourceMappingURL=setPaths.js.map