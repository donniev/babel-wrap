/**
 * module index
 *
 */
'use strict';
var u = require("./utilities/index");
module.exports =(gulp,ns,configFile)=> {
	if(ns && !u.endsWith(ns,".")){
		ns=ns+".";
	}
	ns=ns||"";
	var plugins = require('gulp-load-plugins')();
	var runSequence = require('run-sequence').use(gulp);
	var paths = require('./setPathsGulp')(configFile);
	gulp.task(ns+'clean', require('./gulp/defaultTasks/clean')(gulp, plugins, paths));
	gulp.task(ns+'copy', require("./gulp/defaultTasks/copy")(gulp, plugins, paths));
	gulp.task(ns+'babelFy', require('./gulp/defaultTasks/babelFy')(gulp, plugins, paths));
	gulp.task(ns+'babelFyIncludes', require('./gulp/defaultTasks/babelFyIncludes')(gulp, plugins, paths));
	gulp.task(ns+'copyIncludes', require('./gulp/defaultTasks/copyIncludes')(gulp, plugins, paths));
	gulp.task(ns+'watch', (cb)=> {
		runSequence(ns+'clean', ns+'copy', ns+'copyIncludes', ns+'babelFy', ns+'babelFyIncludes', [ns+'watchAll', ns+'watchBabelFy', ns+'watchBabelFyIncludes', ns+'watchCopyIncludes'], cb);
	});
	gulp.task(ns+'watchBabelFy', ()=> {
		var watcher = gulp.watch(paths.src.babelFy, {interval: 500}, (event)=> {
			if (paths.babeloptions.sourceMaps) {
				return gulp.src(event.path, {base: paths.base})
					.pipe(plugins.sourcemaps.init())
					.pipe(plugins.babel(paths.babeloptions))
					.pipe(plugins.sourcemaps.write('.'))
					.pipe(plugins.debug())
					.pipe(gulp.dest(paths.dest));
			} else {
				return gulp.src(event.path, {base: paths.base})
					.pipe(plugins.babel(paths.babeloptions))
					.pipe(plugins.debug())
					.pipe(gulp.dest(paths.dest));
			}
		});
	});
	gulp.task(ns+'watchBabelFyIncludes', ()=> {
		var watcher = gulp.watch(paths.src.babelFyIncludes, {interval: 500}, function (event) {
			if (paths.babeloptions.sourceMaps) {
				if (paths.debug) {
					return gulp.src(event.path, {base: paths.base})
						.pipe(plugins.sourcemaps.init())
						.pipe(plugins.babel(paths.babeloptions))
						.pipe(plugins.sourcemaps.write('.'))
						.pipe(plugins.debug())
						.pipe(gulp.dest(paths.dest));
				} else {
					return gulp.src(event.path, {base: paths.base})
						.pipe(plugins.sourcemaps.init())
						.pipe(plugins.babel(paths.babeloptions))
						.pipe(plugins.sourcemaps.write('.'))
						.pipe(gulp.dest(paths.dest));
				}
			} else {
				if (paths.debug) {
					return gulp.src(event.path, {base: paths.base})
						.pipe(plugins.babel(paths.babeloptions))
						.pipe(plugins.debug())
						.pipe(gulp.dest(paths.dest));
				} else {
					return gulp.src(event.path, {base: paths.base})
						.pipe(plugins.babel(paths.babeloptions))
						.pipe(gulp.dest(paths.dest));
				}
			}
		});
	});
	gulp.task(ns+'watchAll', ()=> {
		gulp.watch(paths.src.nowatch, {interval: 500}, function (event) {
			if (paths.debug) {
				gulp.src(event.path, {base: paths.base}).pipe(plugins.debug()).pipe(gulp.dest(paths.dest));
			} else {
				gulp.src(event.path, {base: paths.base}).pipe(gulp.dest(paths.dest));
			}
		});
	});
	gulp.task(ns+'watchCopyIncludes', ()=> {
		gulp.watch(paths.src.copyIncludes, {interval: 500}, function (event) {
			if (paths.debug) {
				gulp.src(event.path, {base: paths.base}).pipe(plugins.debug()).pipe(gulp.dest(paths.dest));
			} else {
				gulp.src(event.path, {base: paths.base}).pipe(gulp.dest(paths.dest));
			}
		});
	});
	gulp.task(ns+'default', [ns+'watch']);
	gulp.task(ns+'copyServer', require('./gulp/defaultTasks/copyOne')(gulp, plugins, paths,"server"));
	gulp.task(ns+'copyServerv4', require('./gulp/defaultTasks/copyOne')(gulp, plugins, paths,"server4"));
	gulp.task(ns+'copyServerv5', require('./gulp/defaultTasks/copyOne')(gulp, plugins, paths,"server5"));
	gulp.task(ns+'copyClient', require('./gulp/defaultTasks/copyOne')(gulp, plugins, paths,"client"));

return paths}
