/**
 * module setPaths
 *
 */
'use strict';
var pth = require("path");
module.exports = ()=> {
	var special = void 0;
	var base = "../../"
	var paths = require("./gulp/modulepathsGulp.json");
	var _debug = require('minimist')(process.argv.slice(2)).debug;
	try {
		special = require(pth.resolve(".", "../../babelwrapConfig.json"));
		var regions = ["copy", "nowatch", "babelFy", "babelFyIncludes", "copyIncludes"];
		regions.forEach((region)=> {
			if (!(special["src"][region] === void 0)) {
				special["src"][region] = special["src"][region].map((key) =>key.indexOf("!") === 0 ? "!" + base + key.substr(1) : base + key);
			}
		});
		special["base"] = base + special["base"];
		special["dest"] = base + special["dest"];
		special["src"]["nowatch"] = special["src"]["copy"].concat(special["src"]["nowatch"]);
		special["debug"] = special["debug"] || _debug;
		return special;
	} catch (ex) {
		console.log(ex);
		paths["src"]["nowatch"] = paths["src"]["copy"].concat(paths["src"]["nowatch"]);
		paths["debug"] = paths["debug"] || _debug;
		return paths
	}
};
//# sourceMappingURL=setPaths.js.map